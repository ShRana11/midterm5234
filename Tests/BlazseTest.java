//Student Name: Sukhwinder Singh
//Student Id : C0736516
import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BlazseTest {
	WebDriver driver;
	final String URL = "http://blazedemo.com/";
	final String DriverPath = "/Users/sukhrana/Desktop/chromedriver";


	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",DriverPath);
		driver = new ChromeDriver();
		driver.get(URL);
	}

	@After
	public void tearDown() throws Exception {
		TimeUnit.SECONDS.sleep(1);  // or Thread.sleep(3000)
		driver.close();
	}

	@Test
	public void Hawaiitest() {
		WebElement button = driver.findElement(By.cssSelector("p > a"));
		button.click();
		
//		String url = driver.getCurrentUrl();
//		System.out.println(url);
		
	}
	@Test
	public void DepartureCitytest() {
		List<WebElement> listOfLinks = driver.findElements(By.cssSelector("form > select option"));
		
	   int numLinks = listOfLinks.size();
	   assertEquals(numLinks,7);
	}
	@Test
	public void AmericaFlighttest() {
		WebElement button = driver.findElement(By.cssSelector("div.container > input"));
		button.click();
		
		List<WebElement> listOfLinks = driver.findElements(By.name("VA12"));	
	   int numLinks = listOfLinks.size();
	   assertEquals(numLinks,1);
	}
	@Test
	public void Bookingtest() {
		WebElement button = driver.findElement(By.cssSelector("div.container > input"));
		button.click();
		
		List<WebElement> listOfLinks = driver.findElements(By.name("VA12").cssSelector("td > input"));	
	   int numLinks = listOfLinks.size();
	   WebElement element = listOfLinks.get(0);
	   element.click();
	   
	   //assertEquals(numLinks,1);
	}
	@Test
	public void randomIdTest() {
		WebElement button = driver.findElement(By.cssSelector("div.container > input"));
		button.click();
		
		List<WebElement> listOfLinks = driver.findElements(By.name("VA12").cssSelector("td > input"));	
	   int numLinks = listOfLinks.size();
	   WebElement element = listOfLinks.get(0);
	    element.click();
		WebElement button1 = driver.findElement(By.cssSelector("label.checkbox + input"));
		button1.click();
		
	  List<WebElement> listOftd = driver.findElements(By.cssSelector("tbody > tr td"));	
	 //  int num = listOfLinks.size();
	  WebElement element1 = listOftd.get(1);
//	   element.click();
	  String t = element1.getText();
	  assertNotEquals(t, "1558026391659");
	   
	   //assertEquals(numLinks,1);
	}



}
