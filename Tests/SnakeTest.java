//Student Name: Sukhwinder Singh
//Student Id : C0736516
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {
	private Snake peter;
	private Snake takis;

	@Before
	public void setUp() throws Exception {
		peter = new Snake("Peter S", 10, "coffee");
		takis = new Snake("Takis Z", 80, "vegetables");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void IsHealthytest() throws Exception {
		//healthy Snake : food = vegetables
		Boolean takisSnakeHealth = takis.isHealthy();
		assertTrue(takisSnakeHealth);
		//unhealthy snake food = coffee
		Boolean peterSnakeHealth = peter.isHealthy();
		assertTrue(peterSnakeHealth);
			
	}
	@Test
	public void fitsInCage() throws Exception {
		//fits in cage : cage = 15, snake = 10;
		Boolean takisSnakeLonglength = takis.fitsInCage(100);
		assertTrue(takisSnakeLonglength);
		//fits in cage : cage = 10, snake = 10;
		Boolean takisSnakeEquallength = takis.fitsInCage(80);
		assertTrue(takisSnakeEquallength);
		//short snake
		Boolean takisSnakeShortlength = takis.fitsInCage(8);
		assertTrue(takisSnakeShortlength);
			
	}

}
